up:
	docker-compose up
down:
	docker-compose down
build:
	docker-compose up --build
install: up
	docker-compose exec node yarn install
update:
	docker-compose exec node npm run migrate
dev: up install update
	docker-compose exec node npm run dev
prod:
	docker-compose exec node npm run build
reset: down build install update
jscs:
	docker-compose -f docker-compose-tools.yml run jscs bash -c "npm install --global eslint-config-airbnb-base && eslint /application/src"
deploy: install update prod
