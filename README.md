# Titi

This is Titi core. 

The core is a nodejs application which can manage a list of available devices.
It also let other applications control its features by openning a websocket/http api.

# Requirements
- Node.js
- Mongodb database
(see package.json to get explicit dependencies tree.)