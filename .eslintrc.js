module.exports = {
  "root": true,
  "extends": "airbnb-base",
  "env": {
    "browser": true,
    "node": true
  },
  "rules": {
    'import/no-unresolved': "off",
    "no-undef": "off",
    "no-param-reassign": "off",
    "import/no-dynamic-require": "off",
    "global-require": "off",
    "no-loop-func": "off"
  }
};
