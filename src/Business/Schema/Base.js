const mongoose = require('mongoose');

module.exports = class Base extends mongoose.Schema {
  constructor(obj = {}, options) {
    obj = Object.assign(obj, {
      name: { type: String, required: true },
      description: { type: String, default: '' },
      image: { type: String, default: '/placeholder.png' },
      created_at: { type: Date, default: Date.now },
      updated_at: { type: Date, default: Date.now },
    });

    super(obj, options);
  }
};
