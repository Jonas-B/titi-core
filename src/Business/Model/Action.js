const mongoose = require('mongoose');
const Base = require('../Schema/Base');

module.exports = mongoose.model('Action', new Base({
  values: { type: [mongoose.Schema.Types.Mixed], default: [] },
  format: { type: String, default: 'text' },
}));
