const mongoose = require('mongoose');
const Base = require('../Schema/Base');

module.exports = mongoose.model('Device', new Base({
  actions: { type: [mongoose.Schema.Types.ObjectId], default: [] },
}));
