const mongoose = require('mongoose');
const Base = require('../Schema/Base');

module.exports = mongoose.model('Room', new Base({
  devices: { type: [mongoose.Schema.Types.ObjectId], default: [] },
}));
