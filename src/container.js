const winston = require('winston');
const { format } = require('logform');
const moment = require('moment');
const migrate = require('migrate');

const Container = require('./Services/Container');
const MongoDatabase = require('./Services/Database/MongoDatabase');
const Http = require('./Services/Http');
const ModelManager = require('./Services/ModelManager');

const container = new Container();

container.set('env', (() => {
  const envVars = {};
  Object.keys(process.env).forEach((envVar) => {
    envVars[envVar] = process.env[envVar];
    global[envVar] = process.env[envVar];
  });
  return envVars;
})());

container.set('migrate', () => new Promise((resolve, reject) => {
  migrate.load({
    stateStore: '.migrate',
    migrationsDirectory: 'storage/migrations',
  }, (loadErr, set) => {
    if (loadErr) {
      container.service('logger').error(err.message);
      reject();
    }
    set.up((upErr) => {
      if (upErr) {
        container.service('logger').error(err.message);
        reject();
      }
      container.service('logger').info('Migrations ok');
      resolve();
    });
  });
}), {
  bootstrap: false,
});

container.set('service.database', new MongoDatabase({
  user: DB_USER,
  pass: DB_PASS,
  host: DB_HOST,
  port: DB_PORT,
  name: DB_NAME,
}), {
  method: 'connect',
});

container.set('service.logger', () => {
  const customTimestamps = format((info) => {
    info.timestamp = moment().format('HH:mm:ss');

    return info;
  });

  const logger = winston.createLogger({
    level: process.env.LOG_LEVEL,
    format: format.combine(
      customTimestamps(),
      format.printf(info => `[${info.timestamp}] ${info.level}: ${info.message}`),
    ),
    transports: [
      new winston.transports.File({ filename: 'storage/log/error.log', level: 'error' }),
      new winston.transports.File({ filename: 'storage/log/combined.log' }),
    ],
  });

  if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
      format: format.combine(
        format.colorize(),
        customTimestamps(),
        format.printf(info => `[${info.timestamp}] ${info.level}: ${info.message}`),
      ),
    }));
  }

  global.console.log = data => logger.info(data);

  Object.keys(winston.config.npm.levels).forEach((level) => {
    global.console[level] = data => logger[level](data);
  });

  return logger;
});

container.set('service.http', new Http({
  port: HTTP_PORT,
}), {
  bootstrap: false,
  method: 'start',
});

container.set('service.models', new ModelManager());

module.exports = container;
