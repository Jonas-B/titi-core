const mongoose = require('mongoose');
const GenericDatabase = require('./GenericDatabase');

module.exports = class MongoDatabase extends GenericDatabase {
  connect() {
    let connectionString = 'mongodb://';

    if (this.database.user != null && this.database.pass != null) {
      connectionString += '{user}:{pass}@';
    }

    connectionString += '{host}:{port}/{name}';

    mongoose.connect(connectionString.format(this.database), { useNewUrlParser: true });
    this.connection = mongoose.connection;

    return new Promise((resolve, reject) => {
      this.connection.on('error', error => reject(error));
      this.connection.once('open', data => resolve(data));
    });
  }
};
