const fs = require('fs');
const path = require('path');

module.exports = class ModelManager {
  constructor(options) {
    this.defaultOptions = {
      folder: path.join(BASE_PATH, 'Business/Model'),
    };
    this.options = Object.assign(this.defaultOptions, options);

    this.models = {};
    fs.readdirSync(this.options.folder).forEach((file) => {
      const modelName = path.parse(file).name.toLowerCase();
      const modelFile = require(path.join(this.options.folder, file));
      this.models[modelName] = modelFile;
    });
  }

  getModel(modelName) {
    return this.models[modelName];
  }
};
