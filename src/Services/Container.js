module.exports = class Container {
  constructor() {
    this.dependencies = {};
  }

  set(name, value, options = {}) {
    this.dependencies[name] = {
      value,
      options,
    };
  }

  get(name) {
    return this.dependencies[name].value;
  }

  service(name) {
    return this.get(`service.${name}`);
  }

  bootstrap(force = false) {
    const promises = [];
    Object.keys(this.dependencies).forEach((name) => {
      if (
        (
          force === false
          && this.dependencies[name].options.bootstrap == null
        )
        || (
          force === true
          && this.dependencies[name].options.bootstrap != null
        )
      ) {
        if (this.dependencies[name].value instanceof Function) {
          this.dependencies[name].value = this.dependencies[name].value(this.dependencies);
        }
        if (this.dependencies[name].value instanceof Promise) {
          promises.push(this.dependencies[name].value);
        }
        if (this.dependencies[name].options.method != null) {
          promises.push(this.get(name)[this.dependencies[name].options.method]());
        }
      }
    });
    return Promise.all(promises);
  }
};
