const express = require('express');
const bodyParser = require('body-parser');

module.exports = class Http {
  constructor(options) {
    this.router = express();
    this.router.use(bodyParser.json());
    this.router.use(bodyParser.urlencoded({ extended: true }));

    this.defaultOptions = {
      port: 8080,
      host: '0.0.0.0',
    };

    this.options = Object.assign(this.defaultOptions, options);

    this.router.use((req, res, next) => {
      const log = '{method} - {originalUrl}, {ip}'.format(req);
      container.service('logger').info(log);
      next();
    });

    this.router.use(express.static('../../storage/media'));
  }

  start() {
    this.router.listen(this.options.port);
    container.service('logger').info('HTTP server listening on port {port}.'.format({
      port: this.options.port,
    }));
  }

  attach(path, router) {
    this.router.use(path, router.router);
  }
};
