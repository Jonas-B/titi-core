const ModelRouter = require('./Http/Router/ModelRouter');

require('./bootstrap')
  .then(() => container.bootstrap(true))
  .then(() => {
    container.service('http').attach('/api', new ModelRouter());

    container.service('logger').info('Titi core succesfully started.');
  })
  .catch(err => container.service('logger').error(err.message || err));
