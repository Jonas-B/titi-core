const express = require('express');

module.exports = class ModelRouter {
  constructor() {
    this.router = express.Router();
    this.modelManager = container.service('models');

    this.router.use('/:schema', (req, res, next) => {
      const model = this.modelManager.getModel(req.params.schema);
      if (model != null) {
        res.locals.model = model;
        next();
      } else {
        res.status(404).end();
      }
    });

    this.router.route('/:schema')
      .get((req, res) => {
        res.locals.model.find({}).then(result => res.send(result));
      })
      .post((req, res) => {
        res.locals.model.create(req.body)
          .then(result => res.send(result))
          .catch(err => res.status(400).send(err));
      });

    this.router.use('/:schema/:id', (req, res, next) => {
      res.locals.model.findById(req.params.id).then((target) => {
        if (target != null) {
          res.locals.target = target;
          next();
        } else {
          res.status(404).end();
        }
      }).catch(() => res.status(404).end());
    });

    this.router.route('/:schema/:id')
      .get((req, res) => {
        res.send(res.locals.target);
      })
      .post((req, res) => {
        res.locals.target.set(req.body).save()
          .then(result => res.send(result))
          .catch(err => res.status(400).send(err));
      })
      .delete((req, res) => {
        res.locals.target.remove()
          .then(result => res.send(result))
          .catch(err => res.status(400).send(err));
      });
  }
};
